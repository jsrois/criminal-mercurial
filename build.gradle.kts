import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import com.moowork.gradle.node.yarn.YarnTask

plugins {
	id("org.springframework.boot") version "2.4.2"
	id("com.github.node-gradle.node") version "2.2.4"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	kotlin("jvm") version "1.4.21"
	kotlin("plugin.spring") version "1.4.21"
}

group = "net.jsrois"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("io.github.bonigarcia:webdrivermanager:4.0.0")
	testImplementation("org.seleniumhq.selenium:selenium-java")
	testImplementation("org.seleniumhq.selenium:selenium-support")
	testImplementation("com.willowtreeapps.assertk:assertk:0.23")
}



node {
	version = "14.15.4" //node version
	yarnVersion = "1.22.5"
	download = true
	yarnWorkDir = file("${project.buildDir}/yarn")
}

tasks.register<YarnTask>("installWebClientDependencies") {
	description = "install all dependencies"
	setWorkingDir(file("${project.projectDir}/web-client"))
}


tasks.register<YarnTask>("compileWebClient") {
	dependsOn("installWebClientDependencies")
	description = "builds the frontend bundle"
	setWorkingDir(file("${project.projectDir}/web-client"))
	args = listOf("build")
}

tasks.register<Copy>("copyWebClient") {
	dependsOn("compileWebClient")
	description = "copies the frontend bundle into the resources folder"
	from("${project.projectDir}/web-client/public/build")
	into("${project.projectDir}/src/main/resources/static/.")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}
