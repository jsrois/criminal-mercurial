package net.jsrois.criminalmercurial.entrypoints

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class EchoController {
    @GetMapping("/echo/{count}")
    fun replyWithTheSamePhrase(@PathVariable count: String): String = "The count is $count"
}