package net.jsrois.criminalmercurial.entrypoints

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
class PageController {
    @GetMapping("/")
    fun index() = "index"
}