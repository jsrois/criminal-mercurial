package net.jsrois.criminalmercurial

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class CriminalMercurialApplication

fun main(args: Array<String>) {
	runApplication<CriminalMercurialApplication>(*args)
}
