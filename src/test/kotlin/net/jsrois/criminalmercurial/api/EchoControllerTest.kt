package net.jsrois.criminalmercurial.api

import assertk.assertThat
import assertk.assertions.isEqualTo
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class EchoControllerTest {

    @Autowired
    private lateinit var testRestTemplate: TestRestTemplate

    @Test
    internal fun `should reply with the same number that is passed`() {
        assertThat(
            testRestTemplate.getForEntity("/echo/15", String::class.java).body
        ).isEqualTo("The count is 15")
    }
}