package net.jsrois.criminalmercurial.acceptance

import assertk.assertThat
import assertk.assertions.isEqualTo
import io.github.bonigarcia.wdm.WebDriverManager
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import java.util.concurrent.TimeUnit

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CounterAcceptanceTest {

    @LocalServerPort
    val port = 0

    val webDriver = loadWebDriver()

    @Test
    @Disabled
    internal fun `everytime you click, the count increases`() {
        webDriver.get("http://localhost:${port}")

        val button = webDriver.getCounterButton()

        assertThat(webDriver.getCounterDisplay().text).isEqualTo("0")

        button.click()

        assertThat(webDriver.getCounterDisplay().text).isEqualTo("1")

        button.click()
        button.click()

        assertThat(webDriver.getCounterDisplay().text).isEqualTo("3")
    }

    private fun WebDriver.getCounterButton() = findElement(By.id("counterButton"))
    private fun WebDriver.getCounterDisplay() = findElement(By.id("counterDisplay"))

    private fun loadWebDriver(): WebDriver {
        WebDriverManager.chromedriver().setup()

        val options = ChromeOptions().apply {
            addArguments(
                "--headless",
                "--no-sandbox",
                "--disable-gpu",
                "--disable-dev-shm-usage"
            )
        }

       return ChromeDriver(options).apply {
            manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS)
        }
    }
}