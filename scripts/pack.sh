#!/usr/bin/env bash

./gradlew clean compileWebClient copyWebClient build
docker build \
    --build-arg JAR_FILE=./build/libs/criminal-mercurial-0.0.1-SNAPSHOT.jar \
    -t jsrois/criminal-mercurial .