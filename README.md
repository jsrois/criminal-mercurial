# Kotlin + Svelte Microservice Template
#### a.k.a. Criminal Mercurial*


This is an example web application that uses springboot with kotlin for the backend and
Svelte for the frontend.



### TODO
-[x] Basic Springboot application that runs in a container
-[ ] Basic end-to-end test and pipeline (maybe in gitlab?)
-[x] Build svelte-based frontend into the page
-[ ] Get things from the backend!
-[ ] Add some security layer?
